import Cviceni from './cviceni';

export default class Analytics extends Cviceni {
  constructor() {
    super();

    this.init();
  }

  init() {
    this.analytics.time.start = {
      time: Math.round(performance.now()),
      date: Date.now(),
    };
    this.analytics.activity.steps[this.analytics.activity.length] = {
      type: 'S',
      time: this.analytics.time.start.time,
      date: Date.now(),
    };
  }

  get data() {
    return this.analytics;
  }

  set data(analytics) {
    this.analytics = analytics;
  }

  get timeSpent() {
    return this.analytics.time.end.time - this.analytics.time.start.time;
  }
}
