/* eslint-disable max-classes-per-file */
// [data-fullscreen]
import Component from './component';
import MagnifyingGlass from './magnifying-glass';
import { imageIsLoaded } from '../lib/utils';

class Fullscreen extends Component {
  constructor($element) {
    super($element);

    this.$fullscreen = document.querySelector('[data-fullscreen="fullscreen"]');
    this.$fullscreenContainer = this.$fullscreen.querySelector('[data-fullscreen="container"]');
    this.$fullscreenTarget = this.$fullscreen.querySelector('[data-fullscreen="target"]');

    this.window = {};
    this.image = {};

    this.window.width = window.innerWidth;
    this.window.height = window.innerHeight;
    this.window.ratio = this.window.width / this.window.height;
  }
}

export default class FullscreenToggle extends Fullscreen {
  constructor($element) {
    super($element);

    this.imageAlternative = this.target.getAttribute('data-fullscreen-image');

    // TODO: do not rely on DOM .nextElementSibling, .firstElementChild, etc…
    this.target.addEventListener('click', (event) => {
      // Check if there is different image for zvetseni
      if (this.imageAlternative) {
        this.$fullscreenContainer.innerHTML = this.target.nextElementSibling.nextElementSibling.outerHTML;
        this.$fullscreenTarget = this.$fullscreenContainer.firstElementChild;
        this.$fullscreenTarget.classList.remove('is-hidden');
      }
      else {
        this.$fullscreenContainer.innerHTML = this.target.nextElementSibling.outerHTML;
        this.$fullscreenTarget = this.$fullscreenContainer.firstElementChild;
      }

      this.$fullscreenTarget.removeAttribute('style');
      this.$fullscreenTarget.removeAttribute('id');

      switch (this.target.nextElementSibling.nodeName.toLowerCase()) {
        case 'img':
          setTimeout(() => {
            imageIsLoaded(this.$fullscreenTarget.currentSrc)
              .then(() => {
                this.image.width = this.$fullscreenTarget.naturalWidth;
                this.image.height = this.$fullscreenTarget.naturalHeight;
                this.image.ratio = this.image.width / this.image.height;

                this.setSize();

                const magnifyingGlass = new MagnifyingGlass(this.$fullscreenTarget);
              });
          }, 10);
          break;

        case 'svg':
          // eslint-disable-next-line no-case-declarations
          const viewbox = this.$fullscreenTarget.getAttribute('viewBox').split(' ');

          [, , this.image.width, this.image.height] = viewbox;
          this.image.ratio = this.image.width / this.image.height;

          this.setSize();

          // eslint-disable-next-line no-case-declarations
          const $svgTexts = this.$fullscreenTarget.querySelectorAll('textarea');
          if ($svgTexts.length > 0) {
            $svgTexts.forEach(($svgText) => {
              const { id } = $svgText;
              $svgText.removeAttribute('id');
              $svgText.value = document.getElementById(id).value;
              $svgText.readOnly = true;
            });
          }
          break;

        default:
          break;
      }

      this.$fullscreen.classList.toggle('hidden');

      this.showFeedback();

      event.preventDefault();
      event.stopPropagation();
    }, false);

  }

  setSize() {
    if (this.image.ratio >= this.window.ratio) {
      this.$fullscreenTarget.style.width = '100%';
      this.$fullscreenTarget.style.height = 'auto';
    }
    else {
      this.$fullscreenTarget.style.width = 'auto';
      this.$fullscreenTarget.style.height = `${this.window.height}px`;
    }
  }
}
