﻿const fsp = require('fs').promises;
const readFilesScript = require('../read-files/read-files-script');
// const getImageColorProperties = require('./get-image-color-properties');
// import { getImageColorProperties } from './get-image-color-properties'

/**
 * Method which add domain color to given data file
 * @param {String} dirname - folder of files
 * @param {String} filename - specific filename
 * @param {Object} data - parsed data
 * @returns {Promise<void>} - bcs async function I have to return Promise
 */
const addColorPropertyIntoActivity = async (dirname, filename, data) => {
  // eslint-disable-next-line global-require
  const getImageColorProperties = await require('./get-image-color-properties');
  data.cviceni.color = await getImageColorProperties(
    `src/img/${data.cviceni.uvodniObrazek}`,
  );

  // =====================Rewrite-the-file=====================
  await fsp.writeFile(
    dirname + filename,
    JSON.stringify(data, null, 2),
    (err) => {
      if (err) return console.error(`Error during saving: ${err}`);
      return true;
    },
  );
};

/**
 * Calling method to read all files in src/data
 */
readFilesScript(
  'src/data//',
  (dirname, filename, content) => {
    // Check if the given data can be parsed into JSON
    try {
      const type = filename.split('.');
      if (type[1] !== 'json') {
        console.log(`File: ${filename} is not a json -> SKIPPING`);
        return;
      }
      const contentParsed = JSON.parse(content);
      addColorPropertyIntoActivity(dirname, filename, contentParsed).then(
        () => {
          console.log(
            `Parameter domain color was added into activity ${filename}`,
          );
        },
      );
      // manageData(dirname, filename, contentParsed);
    }
    catch (e) {
      console.log(
        `Error during saving object in activity: ${filename}\n${e}\n`,
      );
    }
  },
  (err) => {
    throw err;
  },
);
